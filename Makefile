
SAMPLES:= \
	samples/paleohispanico_septentrional_rapida.pdf \
	samples/paleohispanico_septentrional_l2_20047.pdf \
	samples/paleohispanico_meridional_rapida.pdf \
	samples/paleohispanico_meridional_l2_20048.pdf

FONTS:= \
	bastida \
	botorrita1 \
	espanca \
	esquirol \
	sudoeste \
	ullastret

all: $(patsubst %,%-all, $(FONTS))

build: $(patsubst %,%-build, $(FONTS))

clean: $(patsubst %,%-clean, $(FONTS))
	-rm samples/*.aux
	-rm samples/*.log
	-rm samples/*.out
	-rm samples/*.pdf
	-rm samples/*.toc

clean-font: $(patsubst %,%-clean-font, $(FONTS))

build-samples: $(patsubst %,%-build-samples, botorrita1 ullastret esquirol) $(SAMPLES)

samples: $(SAMPLES)

%-all: tipos/%/Makefile
	$(MAKE) -C $$(dirname $<)

%-build: tipos/%/Makefile
	$(MAKE) -C $$(dirname $<) build

%-clean: tipos/%/Makefile
	$(MAKE) -C $$(dirname $<) clean

%-clean-font: tipos/%/Makefile
	$(MAKE) -C $$(dirname $<) clean-font

%-build-samples: tipos/%/Makefile
	$(MAKE) -C $$(dirname $<) build-samples

samples/paleohispanico_meridional_%.pdf: samples/paleohispanico_meridional_%.tex samples/l2_20048_comb_imprenta.tex samples/l2_20048_comb_placa.tex build
	(cd samples && xelatex -interaction=nonstopmode $(notdir $<))

samples/paleohispanico_septentrional_%.pdf: samples/paleohispanico_septentrional_%.tex samples/l2_20047_comb_imprenta.tex samples/l2_20047_comb_placa.tex build
	(cd samples && xelatex -interaction=nonstopmode $(notdir $<))

archive: clean-font clean build samples
	zip paleohispanico_$$(git describe --tags).zip \
		LICENSE \
		samples/*.pdf \
		$(patsubst %,tipos/%/build/*.otf, $(FONTS)) \
		$(patsubst %,tipos/%/*.tec, $(FONTS)) \
		$(patsubst %,tipos/%/*.map, $(FONTS))
