#!/usr/bin/env python
import sys


def main():
    text = sys.argv[1]
    codes = []
    for char in text:
        codes.append(f"U+{ord(char):04x}".upper())

    print(" ".join(codes))


if __name__ == "__main__":
    main()
