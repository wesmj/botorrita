#!/usr/bin/env python
import sys


if __name__ == "__main__":
    with open(sys.argv[1]) as fd:
        txt_a = fd.read().rstrip("\n")
    with open(sys.argv[2]) as fd:
        txt_b = fd.read().rstrip("\n")
    with open(sys.argv[3]) as fd:
        nam = {}
        for line in fd:
            line = line.strip()
            if not line:
                continue
            k, v = line.split(" ", 1)
            nam[k.lower()] = v

    print("A: ", "|".join("%s(%s)" % (x, str(hex(ord(x)))) for x in txt_a))
    out = []
    for char in txt_b:
        key = str(hex(ord(char))).lower()
        out.append(nam.get(key, key))

    print("B: ", "|".join(out))
