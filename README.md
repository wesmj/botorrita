
Tipos de letras inspirados en inscripciones iberas y celtiberas.

Por cada inscripción usada como origen se presentan dos variantes:

- Placa: representa una ocurrencia del glifo en la inscripción.
- Imprenta: presenta glifos de estilo regular.


Los tipos se presentan en dos rangos de códigos:

- Rápida: usa el rango de los tipos de letras latinas, y usa ligaduras para mostrar los glifos silábicos.
- L2/20-047: usa los códigos Unicode de la propuesta L2/20-047 para las escrituras paleohispánicas septentrionales.


